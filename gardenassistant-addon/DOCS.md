# Home Assistant Community Add-on: Gardenassistant

[Gardenassistant](https://gitlab.com/stefanfellinger/gardenassistant) - A simple application that helps you manage your hobby garden.
Some of it's features are:

- Define plants with their good and bad neighbours
- Define your planting beds 
- Manage seeding and harvesting
- Image library

## Installation

The installation of this add-on is pretty straightforward and not different in
comparison to installing any other Home Assistant add-on.

1. Search for the "Gardenassistant" add-on in the Supervisor add-on store.
1. Install the "Gardenassistant" add-on.
1. Start the "Gardenassistant" add-on.
1. Check the logs of the "Gardenassistant" add-on to see if everything went well.
1. Click on the "OPEN WEB UI" button to get into the interface of Gardenassistant.
1. Enjoy the add-on!

## Configuration

**Note**: _Remember to restart the add-on when the configuration is changed._

Example add-on configuration:

```yaml
spring.datasource.url: "jdbc:h2:/data/garden-assistant;DB_CLOSE_ON_EXIT=FALSE"
```

**Note**: _This is just an example, don't copy and paste it! Create your own!_

### Option: `spring.datasource.url`

The `spring.datasource.url` option controls the datasource url.


## Known issues and limitations


## Changelog & Releases

This repository keeps a change log using [GitHub's releases][releases]
functionality.

Releases are based on [Semantic Versioning][semver], and use the format
of `MAJOR.MINOR.PATCH`. In a nutshell, the version will be incremented
based on the following:

- `MAJOR`: Incompatible or major changes.
- `MINOR`: Backwards-compatible new features and enhancements.
- `PATCH`: Backwards-compatible bugfixes and package updates.

## Support

Got questions?

You have several options to get them answered:

- You can [open an issue here](https://gitlab.com/stefanfellinger/gardenassistant/-/issues) GitHub.
- Send me a mail to [info.gardenassistant](mailto://info.gardenassistant@gmail.com)
- The [Home Assistant Discord chat server](https://discord.com/invite/c5DvZ4e) for general Home
  Assistant discussions and questions.
- The Home Assistant [Community Forum](https://community.home-assistant.io/).


## Authors & contributors

The original setup of this repository is by [Stefan Fellinger](mailto://info.gardenassistant@gmail.com).

## Icons Usage
Below you will find a list with makers of icons used by the application. Many credits to:

* Icons made by Freepik (https://www.freepik.com) from https://www.flaticon.com/

## License

MIT License

Copyright (c) 2021 Stefan Fellinger

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.