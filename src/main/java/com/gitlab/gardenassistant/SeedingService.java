package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/seedEntries")
@RequiredArgsConstructor
public class SeedingService {
    private final SeedEntryRepository seedEntryRepository;
    private final SeedEntryHistoryRepository seedEntryHistoryRepository;
    private final PlantDetailProvider plantDetailProvider;
    private final PlantingBedRepository plantingBedRepository;

    @DeleteMapping("{id}")
    public void deleteSeedEntry(@PathVariable("id") final Long id) {
        this.seedEntryRepository.deleteById(id);
    }

    @PostMapping
    public SeedEntry saveSeedEntry(@RequestBody final SeedEntry seeding) {
        SeedEntry result = this.seedEntryRepository.saveAndFlush(seeding);
        this.seedEntryHistoryRepository.saveAndFlush(new SeedEntryHistory(seeding));
        return result;
    }

    @GetMapping("plant/{plantId}")
    public List<SeedEntry> getSeedEntriesByPlantId(@PathVariable("plantId") final Long plantId) {
        return this.seedEntryRepository.findAllByPlantId(plantId)
                .stream()
                .collect(Collectors.toList());
    }

    @GetMapping("details/plant/{plantId}")
    public List<SeedingDetail> getSeedingsDetailsByPlantId(@PathVariable("plantId") final Long plantId) {
        return this.seedEntryRepository.findAllByPlantId(plantId)
                .stream()
                .map(this::getSeedingDetail)
                .collect(Collectors.toList());
    }

    @GetMapping("details/plantingBed/{plantingBedId}")
    public List<SeedingDetail> getSeedingsDetailsByPlantingBedId(@PathVariable("plantingBedId") final Long plantingBedId) {
        return this.seedEntryRepository.findAllByPlantingBedIdOrderByVerticalPositionDescHorizontalPositionDesc(plantingBedId)
                .stream()
                .map(this::getSeedingDetail)
                .collect(Collectors.toList());
    }

    @GetMapping("details")
    public List<SeedingDetail> getSeedingsDetails() {
        return this.seedEntryRepository.findAll()
                .stream()
                .map(this::getSeedingDetail)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}/details")
    public List<SeedingDetail> getSeedingDetail(@PathVariable("id") final Long id) {
        return this.seedEntryRepository.findById(id)
                .stream()
                .map(this::getSeedingDetail)
                .collect(Collectors.toList());
    }

    private SeedingDetail getSeedingDetail(final SeedEntry seeding) {
        PlantDetail plantDetail = plantDetailProvider.getPlantDetail(seeding.getPlantId());
        PlantingBed plantingBed = plantingBedRepository.findById(seeding.getPlantingBedId()).orElseThrow();
        return new SeedingDetail(seeding, plantDetail, plantingBed);
    }
}
