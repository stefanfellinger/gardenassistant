package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;

@ApiModel("Defines the location requirement from sunny very try to wet.")
public enum Location {
    SUNNY_VERY_TRY,
    SUNNY,
    SHADY,
    PARTIALLY_SHADY,
    WET
}
