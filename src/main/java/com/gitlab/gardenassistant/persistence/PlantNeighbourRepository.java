package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlantNeighbourRepository extends JpaRepository<PlantNeighbour, Long> {
    void deleteAllByFirstPlantIdAndSecondPlantId(long firstPlantId, long secondPlantId);
    List<PlantNeighbour> findAllByFirstPlantIdAndGoodNeighbour(long plantId, boolean goodNeighbour);
    List<PlantNeighbour> findAllBySecondPlantIdAndGoodNeighbour(long plantId, boolean goodNeighbour);
}
