package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SeedEntryHistoryRepository extends JpaRepository<SeedEntryHistory, Long> {
}

