package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;

/**
 * Definition Nährstoffbedarf:
 * <ul>
 *     <li>LOW - Schwachzehrer</li>
 *     <li>MIDDLE - Mittelzehrer</li>
 *     <li>HIGH - Starkzehrer</li>
 * </ul>
 */
@ApiModel("Defines the nutritional requirement by low, middle or high.")
public enum NutritionalNeeds {
    LOW,
    MIDDLE,
    HIGH
}
