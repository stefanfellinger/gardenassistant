package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlantingBedRepository extends JpaRepository<PlantingBed, Long> {
}
