package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlantFamilyRepository extends JpaRepository<PlantFamily, Long> {
}
