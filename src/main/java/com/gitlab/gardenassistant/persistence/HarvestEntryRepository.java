package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HarvestEntryRepository extends JpaRepository<HarvestEntry, Long> {
    List<HarvestEntry> findAllByPlantIdOrderByDateDesc(Long plantId);
    List<HarvestEntry> findAllByPlantingBedIdOrderByDateDesc(Long plantId);
}
