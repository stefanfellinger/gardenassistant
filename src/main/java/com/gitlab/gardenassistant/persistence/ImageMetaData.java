package com.gitlab.gardenassistant.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class ImageMetaData {
    @Id
    @GeneratedValue
    private Long id;

    private String refType;

    private Long refId;

    private String contentType;

    private String fileName;

    public ImageMetaData(String refType, Long refId, String contentType, String fileName) {
        this.refType = refType;
        this.refId = refId;
        this.contentType = contentType;
        this.fileName = fileName;
    }
}
