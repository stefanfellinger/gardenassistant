package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ImageMetaDataRepository extends JpaRepository<ImageMetaData, Long> {
    Optional<ImageMetaData> findOneByRefTypeAndRefId(String refType, Long refId);
    List<ImageMetaData> findAllByRefTypeAndRefId(String refType, Long refId);
}
