package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApiModel("Model that defines planting bed attributes.")
@Entity
@Data
@NoArgsConstructor
public class PlantingBed {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private int verticalSize;
    private int horizontalSize;

    public PlantingBed(PlantingBed plantingBed) {
        this.id = plantingBed.getId();
        this.name = plantingBed.getName();
        this.verticalSize = plantingBed.getVerticalSize();
        this.horizontalSize = plantingBed.getHorizontalSize();
    }
}
