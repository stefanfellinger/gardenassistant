package com.gitlab.gardenassistant.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.ZonedDateTime;

@Entity
@Data
@NoArgsConstructor
public class SeedEntryHistory {
    @Id
    private Long id;
    private long plantingBedId;
    private long plantId;
    private ZonedDateTime date;
    private int verticalPosition;
    private int horizontalPosition;
    private int numberOfSeeds;

    public SeedEntryHistory(SeedEntry seeding) {
        this.id = seeding.getId();
        this.plantingBedId = seeding.getPlantingBedId();
        this.plantId = seeding.getPlantId();
        this.date = seeding.getDate();
        this.verticalPosition = seeding.getVerticalPosition();
        this.horizontalPosition = seeding.getHorizontalPosition();
        this.numberOfSeeds = seeding.getNumberOfSeeds();
    }
}
