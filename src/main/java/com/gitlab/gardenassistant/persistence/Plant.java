package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@ApiModel("Model that defines plant attributes.")
@Entity
@Data
@NoArgsConstructor
public class Plant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private int seedingStartMonth;
    private Integer distance;
    private int verticalDistance;
    private int horizontalDistance;
    private BigDecimal sowingDepth;
    private int daysFromSeedUntilHarvest;
    @Enumerated(EnumType.STRING)
    private Watering watering;
    @Enumerated(EnumType.STRING)
    private Location location;
    @OneToOne(cascade = CascadeType.ALL)
    private PlantFamily family;
    @Enumerated(EnumType.STRING)
    private NutritionalNeeds nutritionalNeeds;

    public Plant(Plant origin) {
        this.id = origin.getId();
        this.name = origin.getName();
        this.seedingStartMonth = origin.getSeedingStartMonth();
        this.verticalDistance = origin.getVerticalDistance();
        this.horizontalDistance = origin.getHorizontalDistance();
        this.sowingDepth = origin.getSowingDepth();
        this.daysFromSeedUntilHarvest = origin.getDaysFromSeedUntilHarvest();
        this.watering = origin.getWatering();
        this.location = origin.getLocation();
        this.family = origin.getFamily();
        this.nutritionalNeeds = origin.getNutritionalNeeds();
        this.distance = origin.getDistance();
    }

    public void setSowingDepth(BigDecimal bigDecimal) {
        this.sowingDepth = Optional.ofNullable(bigDecimal)
                .map(value -> value.setScale(2, RoundingMode.HALF_DOWN))
                .orElse(BigDecimal.ZERO);
    }

    public int getDistance() {
        return Optional.ofNullable(distance).orElse(this.horizontalDistance);
    }
}
