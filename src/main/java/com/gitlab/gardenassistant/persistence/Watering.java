package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;

/**
 * Definition Wasserbedarf
 */
@ApiModel("Defines the water requirement scale from low to high!")
public enum Watering {
    LOW,
    MIDDLE,
    HIGH
}
