package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.ZonedDateTime;

@ApiModel("Model that defines a harvest entry with attributes.")
@Entity
@Data
@NoArgsConstructor
public class HarvestEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private long plantingBedId;
    private long plantId;
    private long seedEntryId;
    private ZonedDateTime date;
    private String text;
    private int amount;

    public HarvestEntry(HarvestEntry harvestEntry) {
        this.id = harvestEntry.getId();
        this.plantingBedId = harvestEntry.getPlantingBedId();
        this.plantId = harvestEntry.getPlantId();
        this.seedEntryId = harvestEntry.getSeedEntryId();
        this.date = harvestEntry.getDate();
        this.text = harvestEntry.getText();
        this.amount = harvestEntry.getAmount();
    }
}
