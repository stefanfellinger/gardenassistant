package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.ZonedDateTime;

@ApiModel("Model that defines a seed entry with attributes.")
@Entity
@Data
@NoArgsConstructor
public class SeedEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private long plantingBedId;
    private long plantId;
    private ZonedDateTime date;
    private int verticalPosition;
    private int horizontalPosition;
    private int numberOfSeeds;

    public SeedEntry(SeedEntry seeding) {
        this.id = seeding.getId();
        this.plantId = seeding.getPlantId();
        this.plantingBedId = seeding.getPlantingBedId();
        this.date = seeding.getDate();
        this.verticalPosition = seeding.getVerticalPosition();
        this.horizontalPosition = seeding.getHorizontalPosition();
        this.numberOfSeeds = seeding.getNumberOfSeeds();
    }
}
