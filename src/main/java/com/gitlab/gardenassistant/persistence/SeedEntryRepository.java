package com.gitlab.gardenassistant.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SeedEntryRepository extends JpaRepository<SeedEntry, Long> {
    List<SeedEntry> findAllByPlantingBedIdOrderByVerticalPositionDescHorizontalPositionDesc(final long plantingBedId);
    List<SeedEntry> findAllByPlantId(final long plantId);
}

