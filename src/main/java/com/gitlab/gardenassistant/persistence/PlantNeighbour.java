package com.gitlab.gardenassistant.persistence;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@ApiModel("Model that defines plant neighbours with attribute good or bad neighbour.")
@Entity
@Data
@NoArgsConstructor
public class PlantNeighbour {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @OneToOne
    private Plant firstPlant;
    @OneToOne
    private Plant secondPlant;
    private boolean goodNeighbour;

    public PlantNeighbour(Plant plant, Plant neighbourPlant, boolean goodNeighbour) {
        this.firstPlant = plant;
        this.secondPlant = neighbourPlant;
        this.goodNeighbour = goodNeighbour;
    }
}
