package com.gitlab.gardenassistant;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "hassiosupervisor", url = "http://supervisor")
public interface HassioSupervisorService {
    @GetMapping(path = "addons/self/info")
    AddonInfo getAddonInfo();

    @GetMapping(path = "addons/self/info")
    ResponseEntity<JsonNode> getAddonInfoJsonNode();

    @lombok.Data
    class AddonInfo {
        private Data data;
    }

    @lombok.Data
    class Data {
        private String ingress_url;
    }
}
