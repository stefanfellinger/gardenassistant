package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.HarvestEntry;
import com.gitlab.gardenassistant.persistence.Plant;
import com.gitlab.gardenassistant.persistence.PlantingBed;
import com.gitlab.gardenassistant.persistence.SeedEntryHistory;
import lombok.Data;

@Data
public class HarvestDetail extends HarvestEntry {
    private PlantingBed plantingBed;
    private Plant plant;
    private SeedEntryHistory seedEntryHistory;

    public HarvestDetail(HarvestEntry harvestEntry, PlantingBed plantingBed, Plant plant, SeedEntryHistory seedEntryHistory) {
        super(harvestEntry);
        this.plantingBed = plantingBed;
        this.plant = plant;
        this.seedEntryHistory = seedEntryHistory;
    }
}
