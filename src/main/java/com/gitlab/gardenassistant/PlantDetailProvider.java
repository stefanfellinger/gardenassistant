package com.gitlab.gardenassistant;

public interface PlantDetailProvider {
    PlantDetail getPlantDetail(final Long id);
}
