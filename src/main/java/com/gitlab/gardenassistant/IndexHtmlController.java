package com.gitlab.gardenassistant;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Liefert die index.html Seite aus. Bei Auslieferung werden zunächst Inhalte ersetzt
 * und das Ergebnis wird für den kompletten Lifecycle der Anwendung als Instanzvariable gecached.
 */
@RestController
@Slf4j
@RequiredArgsConstructor
public class IndexHtmlController {
    private static final Pattern BASE_HREF_PATTERN = Pattern.compile("(\\<base href=\")(/)(\"\\>)");
    private static final Pattern SRC_PATTERN = Pattern.compile("((src=\")([^\"]*)(\"))");

    private final HassioSupervisorService hassioSupervisorService;

    @Value("classpath:public/index.html")
    private Resource indexHtmlResource;
    private String cachedIndexHtml;

    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String getIndexHtml() throws IOException {
        if(cachedIndexHtml == null) {
            String baseUrl = "";
            try {
                log.info("Hassio Self Info {}", hassioSupervisorService.getAddonInfoJsonNode().getBody());
                HassioSupervisorService.AddonInfo addonInfo = hassioSupervisorService.getAddonInfo();
                baseUrl = addonInfo.getData().getIngress_url();
            } catch (RuntimeException e) {
                log.error("Cannot get ingress url from hassio!", e);
            }

            cachedIndexHtml = loadIndexHtmlContent(baseUrl);
            log.info("Caching index html...");
            log.info(cachedIndexHtml);
        }
        return cachedIndexHtml;
    }

    private String loadIndexHtmlContent(String baseUrl) throws IOException {
        final String result;
        try(BufferedReader inputStreamReader = new BufferedReader(new InputStreamReader(indexHtmlResource.getInputStream()))) {
            result = inputStreamReader.lines()
                    .map(s -> this.mapLine(s, baseUrl))
                    .collect(Collectors.joining());

        }
        return result;
    }

    private String mapLine(String nextLine, String baseUrl) {
        final String result;
        if(StringUtils.hasText(baseUrl)) {
            final Matcher srcMatcher = SRC_PATTERN.matcher(nextLine);
            final Matcher baseHrefMatcher = BASE_HREF_PATTERN.matcher(nextLine);
            if(srcMatcher.find()) {
                result = srcMatcher.replaceAll("$2" + baseUrl + "/$3$4");
            } else if(baseHrefMatcher.find()) {
                result = baseHrefMatcher.replaceFirst("$1" + baseUrl + "$2$3");

            } else {
                result = nextLine;
            }
        } else {
            result = nextLine;
        }

        return result;
    }
}
