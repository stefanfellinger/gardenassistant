package com.gitlab.gardenassistant;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.gardenassistant.persistence.Plant;
import com.gitlab.gardenassistant.persistence.PlantNeighbour;
import com.gitlab.gardenassistant.persistence.PlantingBed;
import com.gitlab.gardenassistant.persistence.SeedEntry;
import feign.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.util.Map;


@SpringBootApplication
@PropertySource(value = "file:/data/options.json", ignoreResourceNotFound = true, factory = Application.JsonLoader.class)
@EnableSwagger2
@EnableFeignClients
@Slf4j
public class Application implements WebMvcConfigurer {

    @Value("${SUPERVISOR_TOKEN:}")
    private String supervisorToken;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args)
                .registerShutdownHook();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH") //
                .exposedHeaders("*");
    }

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return new RepositoryRestConfigurer() {
            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry registry) {
                config.exposeIdsFor(Plant.class, PlantingBed.class, PlantNeighbour.class, SeedEntry.class);
                config.setBasePath("/api");
                registry.addMapping("/api")
                        .allowedOrigins("*") //
                        .allowedMethods("OPTIONS", "HEAD", "GET", "PUT", "POST", "DELETE", "PATCH") //
                        .allowedHeaders("*") //
                        .exposedHeaders("*");
            }
        };
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("/api/.*"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Garden Assistant API")
                .description("Public Garden Assistant API")
                .build();
    }

    /**
     * @return interceptor that adds Hassio bearer Authorization Header Using SUPERVISOR_TOKEN from env variable
     */
    @Bean
    public RequestInterceptor oAuth2FeignRequestInterceptor() {
        final String headerValue = String.format("Bearer %s", supervisorToken);
        return (RequestInterceptor) requestTemplate -> requestTemplate.header("Authorization", headerValue);
    }

    /**
     * Used to load options.json from Hassio Addon Config
     */
    public static class JsonLoader implements PropertySourceFactory {

        @Override
        public org.springframework.core.env.PropertySource<?> createPropertySource(String name,
                                                                                   EncodedResource resource) throws IOException {
            Map readValue = new ObjectMapper().readValue(resource.getInputStream(), Map.class);
            return new MapPropertySource("json-source", readValue);
        }

    }
}
