package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.PlantingBed;
import com.gitlab.gardenassistant.persistence.SeedEntry;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SeedingDetail extends SeedEntry {
    private PlantingBed plantingBed;
    private PlantDetail plantDetail;

    public SeedingDetail(SeedEntry seeding, PlantDetail plantDetail, PlantingBed plantingBed) {
        super(seeding);
        this.plantingBed = new PlantingBed(plantingBed);
        this.plantDetail = plantDetail;
    }
}
