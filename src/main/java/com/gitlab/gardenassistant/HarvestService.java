package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.*;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("harvestEntries")
@RequiredArgsConstructor
@Transactional
public class HarvestService {
    private final String HARVEST_IMAGE_IMAGE_REF_TYPE = "harvest_image";

    private final HarvestEntryRepository harvestEntryRepository;
    private final PlantRepository plantRepository;
    private final PlantingBedRepository plantingBedRepository;
    private final SeedEntryRepository seedEntryRepository;
    private final SeedEntryHistoryRepository seedEntryHistoryRepository;
    private final ImageStorageService imageStorageService;

    @PostMapping
    public HarvestEntry saveHarvestEntry(@RequestBody HarvestEntry harvestEntry) {
        return harvestEntryRepository.saveAndFlush(harvestEntry);
    }

    @GetMapping
    public List<HarvestEntry> getHarvestEntryList() {
        return harvestEntryRepository.findAll();
    }

    @GetMapping("{id}")
    public HarvestEntry getHarvestEntry(@PathVariable("id") final Long id) {
        return harvestEntryRepository.findById(id).orElseThrow();
    }

    @GetMapping("details/plant/{plantId}")
    public List<HarvestDetail> getHarvestDetailsByPlantId(@PathVariable("plantId") final Long plantId) {
        return this.harvestEntryRepository.findAllByPlantIdOrderByDateDesc(plantId)
                .stream()
                .map(this::getHarvestDetail)
                .collect(Collectors.toList());
    }

    @GetMapping("details")
    public List<HarvestDetail> getHarvestDetails() {
        return this.harvestEntryRepository.findAll()
                .stream()
                .map(this::getHarvestDetail)
                .collect(Collectors.toList());
    }

    @GetMapping("details/plantingBed/{plantingBedId}")
    public List<HarvestDetail> getHarvestDetailsByPlantingBedId(@PathVariable("plantingBedId") final Long plantingBedId) {
        return this.harvestEntryRepository.findAllByPlantingBedIdOrderByDateDesc(plantingBedId)
                .stream()
                .map(this::getHarvestDetail)
                .collect(Collectors.toList());
    }

    private HarvestDetail getHarvestDetail(HarvestEntry harvestEntry) {
        Plant plant = plantRepository.findById(harvestEntry.getPlantId()).orElseThrow();
        PlantingBed plantingBed = plantingBedRepository.findById(harvestEntry.getPlantingBedId()).orElse(null);
        SeedEntryHistory seedEntryHistory = seedEntryHistoryRepository.findById(harvestEntry.getSeedEntryId())
                .orElse(null);
        return new HarvestDetail(harvestEntry, plantingBed, plant, seedEntryHistory);
    }

    @PostMapping("{id}/images")
    public void saveImage(@PathVariable("id") final Long id, @RequestParam("file") MultipartFile file) {
        imageStorageService.saveImage(HARVEST_IMAGE_IMAGE_REF_TYPE, id, file);
    }

    @GetMapping(value = "{id}/images")
    public List<String> getImages(@PathVariable("id") final Long id) throws IOException {
        return imageStorageService.getFileNames(HARVEST_IMAGE_IMAGE_REF_TYPE, id);
    }
}
