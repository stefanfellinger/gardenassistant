package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.Plant;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PlantDetail extends Plant {
    private List<Plant> goodNeighbours;
    private List<Plant> badNeighbours;

    public PlantDetail(Plant plant, List<Plant> goodNeighbours, List<Plant> badNeighbours) {
        super(plant);
        this.goodNeighbours = goodNeighbours;
        this.badNeighbours = badNeighbours;
    }
}
