package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.ImageMetaData;
import com.gitlab.gardenassistant.persistence.ImageMetaDataRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@Slf4j
@Transactional
public class ImageStorageService {
    private final ImageMetaDataRepository imageMetaDataRepository;

    @Value("${dataPath:/data/images}")
    private String dataPath;

    @GetMapping("/images/{fileName}")
    public ResponseEntity<Resource> getImage(@PathVariable("fileName") final String fileName) throws IOException {
        return Optional.of(fileName)
                .map(this::resolveFileName)
                .map(FileSystemResource::new)
                .map(fileSystemResource -> (ResponseEntity) ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileSystemResource.getFilename())).body(fileSystemResource))
                .orElse(ResponseEntity.noContent().build());
    }

    private Path resolveFileName(final String fileName) throws RuntimeException {
        final Path result = Paths.get(dataPath);
        if(Files.notExists(result)) {
            try {
                Files.createDirectory(result);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return result.resolve(fileName);
    }

    public ResponseEntity<Resource> getImage(final String imageRefType, final Long imageRefId) throws IOException {
        Optional<ImageMetaData> optionalImageMetaData = imageMetaDataRepository.findOneByRefTypeAndRefId(imageRefType, imageRefId);

        return optionalImageMetaData
                .map(imageMetaData -> resolveFileName(imageMetaData.getFileName()))
                .filter(Files::exists)
                .map(FileSystemResource::new)
                .map(fileSystemResource -> (ResponseEntity) ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileSystemResource.getFilename()))
                        .header(HttpHeaders.CONTENT_TYPE, optionalImageMetaData.get().getContentType())
                        .body(fileSystemResource))
                .orElse(ResponseEntity.noContent().build());
    }

    public List<String> getFileNames(final String imageRefType, final Long imageRefId) throws IOException {
        return imageMetaDataRepository.findAllByRefTypeAndRefId(imageRefType, imageRefId)
                .stream()
                .map(ImageMetaData::getFileName)
                .collect(Collectors.toList());
    }

    public void saveImage(final String imageRefType, final Long imageRefId, MultipartFile file) {
        String name = file.getOriginalFilename();
        String contentType = file.getContentType();
        String fileName = String.format("%s-%s-%s", imageRefType, imageRefId, name);
        Path filepath = resolveFileName(fileName);
        try {
            ImageMetaData imageMetaData = imageMetaDataRepository.saveAndFlush(new ImageMetaData(imageRefType, imageRefId, contentType, fileName));
            file.transferTo(filepath);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException("Error storing file!");
        }
    }

}
