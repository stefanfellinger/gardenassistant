package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/plants")
@RequiredArgsConstructor
@Transactional
@Slf4j
public class PlantService implements PlantDetailProvider {
    private final String PLANT_ICON_IMAGE_REF_TYPE = "plant_icon";
    private final PlantRepository plantRepository;
    private final PlantFamilyRepository plantFamilyRepository;
    private final PlantNeighbourRepository plantNeighbourRepository;
    private final ImageStorageService imageStorageService;

    private List<Plant> getNeighbours(Long plantId, boolean goodNeighbour) {
        final Stream<Plant> byFirstPlantStream = plantNeighbourRepository.findAllByFirstPlantIdAndGoodNeighbour(plantId, goodNeighbour)
                .stream()
                .map(PlantNeighbour::getSecondPlant);
        final Stream<Plant> bySecondPlantStream = plantNeighbourRepository.findAllBySecondPlantIdAndGoodNeighbour(plantId, goodNeighbour)
                .stream()
                .map(PlantNeighbour::getFirstPlant);
        return Stream.concat(byFirstPlantStream, bySecondPlantStream)
                .collect(Collectors.toList());
    }

    private void setNeighbour(final Long id, final Plant neighbourPlant, boolean goodNeighbour) {
        final Plant plant = plantRepository.findById(id).orElseThrow();
        plantNeighbourRepository.save(new PlantNeighbour(plant, neighbourPlant, goodNeighbour));
    }

    public PlantDetail getPlantDetail(final Long plantId) {
        final Plant plant = plantRepository.findById(plantId).orElseThrow();

        final List<Plant> goodNeighbours = getNeighbours(plantId, true);
        final List<Plant> badNeighbours = getNeighbours(plantId, false);
        return new PlantDetail(plant, goodNeighbours, badNeighbours);
    }

    @PostMapping("/families")
    public PlantFamily savePlantFamily(@RequestBody PlantFamily plantFamily) {
        return this.plantFamilyRepository.saveAndFlush(plantFamily);
    }

    @GetMapping("/families")
    public List<PlantFamily> getPlantFamilies() {
        return this.plantFamilyRepository.findAll();
    }

    @DeleteMapping("/families/{id}")
    public void deletePlantFamily(@PathVariable("id") final Long id) {
        this.plantFamilyRepository.deleteById(id);
    }

    @PostMapping
    public Plant savePlant(@RequestBody final Plant plant) {
        return plantRepository.saveAndFlush(plant);
    }

    @GetMapping
    public List<Plant> getPlants() {
        return plantRepository.findAll();
    }

    @GetMapping("{id}")
    public Plant getPlant(@PathVariable("id") final Long id) {
        return plantRepository.getOne(id);
    }

    @DeleteMapping("{id}")
    public void deletePlant(@PathVariable("id") final Long id) {
        plantRepository.deleteById(id);
    }

    @PostMapping("{id}/good-neighbours")
    public void setGoodNeighbour(@PathVariable("id") final Long id, @RequestBody final Plant neighbourPlant) {
        setNeighbour(id, neighbourPlant, true);
    }

    @PostMapping("{id}/bad-neighbours")
    public void setBadNeighbour(@PathVariable("id") final Long id, @RequestBody final Plant neighbourPlant) {
        setNeighbour(id, neighbourPlant, false);
    }

    @GetMapping("{id}/good-neighbours")
    public List<Plant> getGoodNeighbours(@PathVariable("id") final Long plantId) {
        return getNeighbours(plantId, true);
    }

    @GetMapping("{id}/bad-neighbours")
    public List<Plant> getBadNeighbours(@PathVariable("id") final Long plantId) {
        return getNeighbours(plantId, false);
    }

    @DeleteMapping("{id}/neighbours/{neighbourId}")
    public void deleteNeighbour(@PathVariable("id") final Long plantId, @PathVariable("neighbourId") final Long neighbourId) {
        plantNeighbourRepository.deleteAllByFirstPlantIdAndSecondPlantId(plantId, neighbourId);
        plantNeighbourRepository.deleteAllByFirstPlantIdAndSecondPlantId(neighbourId, plantId);
    }

    @PostMapping("{id}/icon")
    public void savePlantIcon(@PathVariable("id") final Long id, @RequestParam("file") MultipartFile file) {
        imageStorageService.saveImage(PLANT_ICON_IMAGE_REF_TYPE, id, file);
    }

    @GetMapping("{id}/icon")
    public ResponseEntity<Resource> getPlantIcon(@PathVariable("id") final Long id) throws IOException {
        return imageStorageService.getImage(PLANT_ICON_IMAGE_REF_TYPE, id);
    }
}
