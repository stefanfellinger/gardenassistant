package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.PlantingBed;
import com.gitlab.gardenassistant.persistence.PlantingBedRepository;
import com.gitlab.gardenassistant.persistence.SeedEntry;
import com.gitlab.gardenassistant.persistence.SeedEntryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/plantingBeds")
@RequiredArgsConstructor
@Transactional
@Slf4j
public class PlantingBedService {
    private final PlantingBedRepository plantingBedRepository;
    private final SeedEntryRepository seedEntryRepository;
    private final PlantDetailProvider plantDetailProvider;

    @PostMapping
    public PlantingBed savePlantingBed(@RequestBody final PlantingBed plantingBed) {
        return plantingBedRepository.saveAndFlush(plantingBed);
    }

    @GetMapping
    public List<PlantingBed> getPlantingBeds() {
        return plantingBedRepository.findAll();
    }

    @GetMapping("{id}")
    public PlantingBed getPlantingBed(@PathVariable("id") final Long id) {
        return plantingBedRepository.getOne(id);
    }

    @DeleteMapping("{id}")
    public void deletePlantingBed(@PathVariable("id") final Long id) {
        List<SeedEntry> seedEntryList = seedEntryRepository.findAllByPlantingBedIdOrderByVerticalPositionDescHorizontalPositionDesc(id);
        seedEntryRepository.deleteInBatch(seedEntryList);
        plantingBedRepository.deleteById(id);
    }

    @GetMapping("{id}/detail")
    public PlantingBedDetail getPlantingBedDetail(@PathVariable("id") final Long id) {
        PlantingBed plantingBed = this.plantingBedRepository.findById(id).orElseThrow();
        List<SeedingDetail> seedingDetailList = seedEntryRepository.findAllByPlantingBedIdOrderByVerticalPositionDescHorizontalPositionDesc(id)
                .stream()
                .map(this::getSeedingDetail)
                .collect(Collectors.toList());

        return new PlantingBedDetail(plantingBed, seedingDetailList);
    }

    private SeedingDetail getSeedingDetail(final SeedEntry seeding) {
        PlantDetail plantDetail = plantDetailProvider.getPlantDetail(seeding.getPlantId());
        PlantingBed plantingBed = plantingBedRepository.getOne(seeding.getPlantingBedId());
        return new SeedingDetail(seeding, plantDetail, plantingBed);
    }
}
