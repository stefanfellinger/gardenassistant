package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.Plant;
import com.gitlab.gardenassistant.persistence.PlantingBed;

public interface RaisedBedCalculator {

    int findVerticalSpace(PlantingBed plantingBed, Plant plant);
    int findHorizontalSpace(PlantingBed plantingBed, Plant plant);
}
