package com.gitlab.gardenassistant;

import com.gitlab.gardenassistant.persistence.PlantingBed;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PlantingBedDetail extends PlantingBed {
    private List<SeedingDetail> seedingDetailList;

    public PlantingBedDetail(PlantingBed plantingBed, List<SeedingDetail> seedingDetailList) {
        super(plantingBed);
        this.seedingDetailList = seedingDetailList;
    }
}
